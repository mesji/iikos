[Do you need Windows?](https://developer.microsoft.com/en-us/windows/downloads/virtual-machines/)

[Do you need Linux?](https://docs.microsoft.com/en-us/windows/wsl/install-win10) (I recommend running Ubuntu 20.04 LTS, and reading [The Complete Guide to Windows Subsystem for Linux 2](https://www.sitepoint.com/wsl2-windows-terminal))

(if you need to update Windows before installing Linux e.g. from 1909 to 2004 and this does not happen automatically with Windows Update, use the [Windows Update Assistant](https://www.microsoft.com/software-download/windows10))

| Date | Topic         |
| ---- | ------------- |
| July 29th | Linux: Login, Files and Directories (chp 1 in [this compendia](https://gitlab.com/erikhje/iikg1001), chp 1,2,3,4 in [textbook](http://www.linuxcommand.org/tlcl.php)) |
| Aug 3rd   | Linux: Redirections and Expansions (chp 2 in [this compendia](https://gitlab.com/erikhje/iikg1001), chp 6,7 in [textbook](http://www.linuxcommand.org/tlcl.php)) |
| Aug 4th   | Linux: Permissions and Processes (chp 3 in [this compendia](https://gitlab.com/erikhje/iikg1001), chp 9,10 in [textbook](http://www.linuxcommand.org/tlcl.php)) |
| Aug 10th  | [PowerShell](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md) (stop before [Remoting](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md#remoting)) |
