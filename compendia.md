[[_TOC_]]

# Introduction to Computer Architecture

## Outcome

##### Learning Outcome

  - Layering/Abstraction

  - Performance

  - Basic hardware overview

**Terminology**

ALU, CU, I/O, bus, MMU, kontroller, firmware, bit vs Byte, data vs
instruksjon, instruksjonssett, mikroarkitektur, register,
AX/BX/CX/DX/SP/BP/IP/IR/FLAG/PSW, adresse, interrupt, interruptrutine,
stack/push/pop, context switch, assembly-direktiv, assembly-merke/label,
byte/word/long/quadword, mnemonic, klokke-hastighet/takt/periode, Hz,
pipeline, micro-operasjoner, out-of-order utførelse, superskalar,
SMT/hyperthreading, von Neumann-flaskehalsen, spatial/temporal locality,
cache line, write through/back cache

## Architecture

##### CPU, Memory, I/O

![image](img/hw-1.png)

A computer architecture composed of a CPU (with CU and ALU), memory,
storage and I/O is called either a [von Neumann architecture or a
Harvard/modified Harvard architecture dependent upon whether you have
multiple buses (to separate data and instructions) or
not](https://en.wikipedia.org/wiki/Harvard_architecture#Contrast_with_von_Neumann_architectures).

De viktigste komponentene, som vi snart skal lære en hel del mer om, er:

  - CPU  
    Central Processing Unit - Hovedprosessoren. Dette er datamaskinens
    "hjerne", og er den enheten som vanligvis er ansvarlig for å kjøre
    programmene vi ønsker å kjøre.
    
      - MMU  
        Memory Management Unit - En sentral del som lar hvert program
        som kjøres få sitt eget minneområde. Denne skal vi se nærmere på
        i kapittel [6](#chap:memman).
    
      - CU  
        Control Unit - styrer dataflyten og får
        operasjoner/instruksjoner til å utføres, det er CU som driver
        fremover prosessoren.
    
      - ALU  
        Arithmetic Logic Unit - utfører instruksjoner, dvs enkle
        regneoperasjoner (aritmetiske og logiske) på binære tall.
    
      - Registere  
        Den minste enhet i datamaskinenen hvor man lagre litt data (her
        lagrer man typisk fra 8 til 64 bit).
        
          - AX, BX, CX, DX, SP, BP, SI, DI  
            Data-registere, holder informasjon om det kjørende
            programmet.
        
          - IP/PC (Instruksjonspeker/Program Counter)  
            inneholder adressen som peker inn i RAM til der neste
            instruksjon som skal utføres befinner seg.
        
          - IR (Instruksjonsregister)  
            inneholder selve instruksjonen som utføres.
        
          - SP (Stackpeker)  
            innehholder adressen til toppen av stacken.
        
          - BP (Basepeker/Framepeker)  
            inneholder adressen til bunnen av nåværende stack frame (dvs
            stacken er delt inn i stack frames, en stack frame skapes
            når du kjør et funksjonskall og den slettes når du
            returnerer fra funksjonen).
        
          - FLAG/PSW (Flagregister/Program Status Word)  
            inneholder kontroll/status informasjon (spesielt kan nevnes
            resultatbit fra regneoperasjoner og to bit som indikerer om
            det kjørende programmet kjører i user mode eller kernel
            mode).

  - Memory/RAM  
    Random Accessible Memory - Hovedminnet til en datamaskin. Dette
    benyttes av CPU til å lagre informasjon den jobber med; samt at den
    leser programmene sine fra minnet.

  - I/O-enheter  
    I/O-enheter er knyttet til datamaskinens sentrale bus, og blir
    benyttet av CPU for å få informasjon ut og inn av datamaskinen.
    Disse består normalt sett av en egen “liten datamaskin” som kalles
    en kontroller som har prosessor, litt minne, noe programvare
    (firmware) og noen grensesnitt (sine egne registere som CPU kan
    skrive til eller lese fra).

### Register

##### Registers

![image](img/register.png)

The AX-register can be used in 8-bit, 16-bit, 32-bit or 64-bit version.
If you see a register starting with `r` you know it’s the 64-bit
version, if it starts with `e` it’s the 32-bit version. We rarely see
the 16-bit or 8-bit versions today.

### Instruction

##### Instructions

Consider these simple C-Programs

    int main() {        int main() {
      int a = 2;          return 2 + 3;
      int b = 3;        }
      int a = a + b;  
      return a;       
    }

These programs can be expressed somewhat like so:

    # Store the value 2 somewhere (called a)
    # Store the value 3 somewhere (called b)
    # Add the value of b to a
    # Return the value of a

Expressed in a language like assembly it might look something like this

    movl eax, 0x2
    movl ebx, 0x3
    addl eax, ebx
    ret

Når man skriver programvare i leselige språk (C, C++, Python, Bash, PHP
etc.) må disse språkene gjennom en "konvertering" før en CPU kan kjøre
programmet. En CPU kan ikke lese C. Denne konverteringen kalles gjerne
for tolking (Python, Bash, PHP) eller kompilering (C, C++).

Når kode kompileres vil kompilatoren analysere koden som er skrevet, og
deretter bryte den ned til små enkeltoperasjoner som CPU kan utføre. I
eksempelet over er to ulike C-program begge beskrevet språklig med de
samme 4 enkeltoperasjonene. Siden en CPU ikke leser språk er disse 4
enkeltoperasjonene beskrevet i ett språk som heter assembly. En linje
assembly tilsvarer en CPU instruksjon.

### ISA

##### Instruction Set Architecture

  - Elec. Engineer: Microarchitecture

  - Comp. Scientist: Instruction Set Architecture (ISA):
    
      - native data types and *instructions*
    
      - *registers*
    
      - addressing mode
    
      - memory architecture
    
      - interrupt and exception handling
    
      - external I/O

Hver CPU type har et bestemt sett med instruksjoner den kan utføre
(instruksjonssettet, som er forskjellig mellom de ulike arkitekturene
f.eks. Intel/AMD X86 som vi skal bruke, Arm (som er i mobiltelefonene og
nettbrettene) og Sun SPARC som var en slager på kraftige
arbeidsstasjoner før). Instruksjonssettet er det vi som programmerere
ser av datamaskinarkitekturen, dvs det vi kan bruke direkte for å
programmere på lavest mulige nivå. Vanligvis bruker vi den symbolske
representasjonen av selve maskininstruksjonene: assemblykode.

Mens vi som informatikere som regel ikke bryr oss om lavere nivå enn
instruksjonssettet, er derimot elektroingeniørene opptatt av
*Mikroarkitekturen* som dreier seg om hvordan instruksjonene faktisk
skal implementeres i elektroniske komponenter for å lage en fysisk CPU.

##### Regular instructions

Some instructions are "always" available:

  - **Moving data** mov

  - **Math functions** add, sub

  - **Function related** call, ret

  - **Jumping** jmp, je, jne

  - **Comparing** cmp

  - **Stack** push, pop

### How CPU works

Basert på det vi kan til nå kan vi tenke oss at det CPU gjør tilsvarer
ca følgende pseudokode:

##### CPU Workflow

    while(not HALT) { # while powered on
      IR=Program[PC]; # fetch instruction pointed to by PC to IR
      PC++;           # increment PC (program counter aka IP)
      execute(IR);    # execute instruction in IR
    }

This is the instruction cycle aka  
*fetch, (decode,) execute* cycle.

### Interrupts

##### CPU Workflow - With interrupts

``` 
    while(not HALT) {
      IR = mem[PC];   # IR = Instruction Register
      PC++;           # PC = Program Counter (register)
      execute(IR);
      if(IRQ) {       # IRQ = Interrupt ReQuest
        savePC();
        loadPC(IRQ);  # Hopper til Interrupt-rutine
      }
    }
```

For at en skal kunne behandle hendelser som ikke kan planlegges på
forhånd åpner man opp for avbrudd, eller "interrupt". I praksis
fungerer dette slik at når ett interrupt kommer vil CPU starte å
behandle dette interruptet istedet for å fortsette på programmet som
kjørte. Når interruptet er behandlet vil CPU gå tilbake til programmet
som kjørte.

##### Registers vs Physical Memory (RAM)

Register contents *belong to the current running program* and the
operating system

There can be *several programs loaded in memory* (this is called
multiprogramming)

## Software

### Compiling

Generelt kan vi betrakte abstraksjonsnivåene i en datamaskin ut fra
følgende figur:

``` 
  høynivå
  A
  |                                               A
  | 4GL: modellering med kodegenerering           |
  |                                            Laget mtp
  | Høynivå progspråk                          mennesker
  | (C,C++,Java,...)    (portabel)    
  |
  | - - (bytecode?) - - - - - - - - - - - - - - - - - - -  
  |
  | Assembly         (ikke portabel)           Laget mtp   
  |                                            maskiner
  | Maskinkode                                    |
  |                                               V
  | (Mikroarkitektur, Mikrooperasjoner)
  |
  | (Digital logikk) 
  V                                               
  lavnivå
```

DEMO1 - Se assemblykode og maskinkode laget av gcc fra C:

``` 
  cat asm-0.c
  gcc -S asm-0.c       # lag assembly kode
  gcc asm-0.c          # lag maskinkode
  cat asm-0.s          # se linjen "ret"
  hexdump -C asm-0     # se ferdiglinket maskinkode som hex
```

DEMO2 - Bruke en disassembler for å lese maskinkode

``` 
  # mapping finnes i http://ref.x86asm.net/coder64-abc.html
  # eller se via innholdsfortegnelsen (ark nr 1000) i
  # http://download.intel.com/products/processor/manual/325383.pdf
  gdb asm-0            # åpne maskinkoden i GNU Debugger
   disassemble /r main  # disassemble og vis tilhørende maskinkode
   set disassembly-flavor intel # vis i Intel syntax istedet
   disassemble /r main
  # alternativt:
  objdump -d asm-0 | less
  # med Intel syntax:
  objdump -d --disassembler-options=intel asm-0 | less
```

Det er lettest å disassemble opcodes som ikke har operander, slik som
`ret`. La oss se hvordan `retq` mappes til C3 i URLn over, dvs søk på C3
og etterhvert finner du en mapping til `retn` som er den samme, dvs en
return av type near (near = innen samme minnesegment, far = return til
et annet minnesegment).

Benytt anledningen til å se litt om diverse gode verktøy som finnes for
å inspisere program / reverse engineering: [Simple Tools and Techniques
for Reversing a binary - bin 0x06
(LiveOverflow)](https://www.youtube.com/watch?v=3NTXFUxcKPc)

### gcc

Man kan benytte gcc til å kompilere C kode til assembly og maskinkode,
samt til å konvertere til/fra assembly/maskinkode.

##### GNU Compiler Collection: gcc

``` 
    gcc -S tmp.c      # from C to assembly
    gedit tmp.s       # edit it
    gcc -o tmp tmp.s  # from assembly to machine code
    ./tmp             # run machine code
    
    # sometimes nice to remove lines with .cfi
    gcc -S -o - tmp.c | grep -v .cfi > tmp.s
    # or avoid .cfi-lines in the first place
    gcc -fno-asynchronous-unwind-tables -S tmp.c
```

### 32 vs 64 bit

##### 32 vs 64 bit code

Same C-code, different assembly and machine code

``` 
    gcc -S asm-0.c      # 64-bit since my OS is 64-bit
    grep push asm-0.s   # print lines containing "push"
    gcc -S -m32 asm-0.c # 32-bit code
    grep push asm-0.s   # print lines containing "push"
```

Difference in instructions and registers

### Syntax

##### Assembly Language

See fig 4.1 (program layout)

``` 
          .file   "asm-0.c"    # DIRECTIVES
          .text
          .globl main
```

``` 
          main:                # LABEL
```

``` 
          push   rbp           # INSTRUCTIONS
          mov    rsp, rbp 
          mov    0, eax
          ret
```

Direktiver starter med et punkt (`.`) og merker (labels) avsluttes med
kolon (`:`).

Assemblykode oversettes til maskinkode med et program som kalles en
assembler (f.eks. GAS, NASM, el.l. program). Assemblykode består av
instruksjoner (som oversettes som oftest en-til-en til maskinkode) og
direktiver som er informasjon til assembleren. I tillegg benyttes også
labels (merker) for å henvis til bestemte deler av koden.

Linje for linje betyr denne assemblykoden:

  - .file "asm-0.c"  
    metainformasjon som sier hvilken kildekode som er opphavet til denne
    koden

  - .text  
    sier at her starter kodedelen (dvs selve instruksjonene i et program
    kalles ofte text, i motsetning til andre deler som f.eks. kan være
    bare data)

  - .globl main  
    sier at main skal være global i scope (synlig for annen kode som
    ikke er i akkurat denne filen, dvs kode som linkes inn, delte
    biblioteker o.l.)

  - main:  
    et label (merke), det er vanlig å kalle hovedfunksjonen i et program
    for main

  - push rbp  
    legger base pointer (aka frame pointer) på stacken

  - mov rsp, rbp  
    setter base pointer (registeret rbp) lik stack pointer (registeret
    rsp)

  - mov 0, eax  
    setter et “general purpose register” eax til å være 0, det er vanlig
    å legge returverdien til et program i eax registeret

  - ret  
    legger den gamle instruksjonspekern som befinner seg på stacken
    tilbale i rip registeret hvis denne finnes slik at den funksjonen
    som kallet meg kan fortsette der den slapp

##### GNU/GAS/AT\&T Syntax

<span><http://en.wikibooks.org/wiki/X86_Assembly/GAS_Syntax></span>

  - instruction/opcode  
    mnemonic

  - mnemonic suffix  
    b (byte), w (word), l (long), q (quadword)

  - operand  
    argument

  - operand prefix  
    % (register), $ (constant/number)

  - address op.  
    `movl -4(%ebp), %eax`  
    “load what is located in address\((\mathrm{ebp}-4)\) into eax”

En mnemonic tilsvarer en maskinkode opcode. Dvs det kalles mnemonic
fordi det er bare et kort navn som brukes istedet for å måtte huske den
egentlig numeriske maskinkode opcode’n.

##### Overview of X86 assembly

<span><http://en.wikipedia.org/wiki/X86_instruction_listings></span>

### Eksempler

:

`asm-0.c` Eksempelet over

`asm-1.c` En ekstra kodelinje fordi 0 skrives til stacken, og deretter
kopieres fra stacken til registeret (MERK: plutselig to skrivinger til
minne som er mye (dvs minst 10x) tregere enn å skrive til et register)

MERK: *parenteser betyr altså at vi prater om en adresse i minne.*

``` 
  diff asm-0.s diff asm-1.s
```

`asm-2.c` Lokale og globale variable, merk at det har dukket opp en
section `.data` eller `.bss`. Data er området der globale variabler som
har en initiell verdi er lagret. Disse verdiene må være lagret i
programfilen. BSS er området der globale variabler uten noen initiell
verdi ligger lagret. Det er nok å bare ha størrelsen på disse variablene
i programfilen, siden de ikke skal ha noen verdi.

Merk at den globale variabelen `j` brukes til å adressere med
utgangspunkt i instruksjonspekern `rip`. Dette er et tilfelle av
PC-relative adressering
(<http://software.intel.com/en-us/articles/introduction-to-x64-assembly>):

> RIP-relative addressing: this is new for x64 and allows accessing data
> tables and such in the code relative to the current instruction
> pointer, making position independent code easier to implement.

Se også <http://en.wikipedia.org/wiki/Addressing_mode#PC-relative_2>

Her ser vi også `add` instruksjonen.

Det området på stacken som en funksjon benytter kalles en *stack frame*,
og består av det området som pekes ut mellom base pointer og stack
pointer. Når en funksjon kalles lagres base pointer unna på stacken og
base pointer settes lik stack pointer, dermed har vi startet en ny stack
frame, og vi kan gå tilbake til forrige stack frame når funksjonen er
ferdig.

Stack frame forklart: [First Stack Buffer Overflow to modify Variable -
bin 0x0C (LiveOverflow)](https://www.youtube.com/watch?v=T03idxny9jE)

DEMO: `asm-3-stack.c`

##### Why Learn Assembly

From Carter (2006) *PC Assembly Language*, page 18:

  - Assembly code *can be faster* and smaller than compiler generated
    code

  - Assembly allows access to *direct hardware features*

  - Deeper understanding of *how computers work*

  - Understand better how *compilers* and high level languages like C
    work

Eksempelvis vil spillprogrammerere ønske å utnytte hardware egenskaper
maksimalt, mens sikkerhetsanalytikere ønsker å benytte assembly-nivå for
effektiv implementering av lavnivå kryptooperasjoner hvor det ofte er
snakk om å flytte bit i et register som en del av en kryptoalgoritme.

La oss sjekke at vi har noenlunde kontroll på [assemblykode,
programutførelse og stack (basert på X86 og C++ kode) og en del
tilhørende kommandolinjeverktøy](https://vimeo.com/21720824).

En siste god link å lese for å få enda en gjennomgang av grunnleggende
assembly er [Understanding C by learning
assembly](https://www.recurse.com/blog/7-understanding-c-by-learning-assembly)

## CPU terminology

##### Important Concepts

  - *Clock* speed/rate

  - *Pipeline*, *Superscalar*

  - Machine instructions into *Micro operations* (\(\mu\mathrm{ops}\))

  - *Out-of-Order* execution

Selve utførelsen av alt som skjer i en datamaskin baseres på en
klokketakt, dvs hvis vi har en klokkehastighet på 1GHz så betyr det at
det kommer en milliard pulser (generert av en oscillator) hvert sekund,
og hver slik pulse skyver utførelsen av instruksjonene et hakk videre.
Litt forenklet kan vi tenke på at CPUn da klarer å utføre en instruksjon
for hver puls (klokkeperiode), som vil si at den bruker et nanosekund
(et milliard’te-dels sekund) på å utføre en instruksjon (dette er ikke
helt presist siden en superskalar CPU kan utføre flere
mikroinstruksjoner hver klokkeperiode).

### Pipeline/Superscalar

##### Pipelined and Superscalar CPU

![image](img/01-07.png)  
<span>(From Tanenbaum “Modern Operating Systems, 2nd ed”)</span>

Utførelsen av en instruksjon skjer i flere steg, som vi kaller for
mikrooperasjoner. For eksempel vil det å legge sammen to tall som ligger
i hvert sitt register kunne bli brutt opp i minst følgende steg:

1.  (fetch) Hente instruksjonen fra minne

2.  (decode) Dekode den, hvilken instruksjon er dette?

3.  (decode) Hente inn registrene som skal legges sammen

4.  (execute) Legge sammen tallene

5.  (execute) Lagre resultatet i ett register

For at CPU skal jobbe mest mulig effektivt lager en derfor egne klart
adskilte enheter der hver av disse mikrooperasjonene utføres, og så lar
vi instruksjonene passere disse enhetene en etter som, som på ett
samlebånd. En slik organisering av CPU kalles for en "pipeline", og er
illustrert i del a av figuren over.

For å gjøre CPU enda mer effektiv, så kan en duplisere deler av
pipelinen slik at man behandler mer enn en instruksjon om gangen. Man
kan for eksempel lage 2 sett med de enhetene som henter instruksjoner
fra RAM og dekoder disse. Man kan og lage flere enheter som kan kjøre
instruksjonene (eksekveringsenheter), slik at det kan utføres flere
instruksjoner i parallell. Da får man en super-skalar arkitektur, som er
illustrert i del b av figuren over.

Moderne prosessorer som er i vanlige datamaskiner er normalt sett en
super-skalar CPU, og eksekveringsenhetene i disse CPU-ene er ofte veldig
spesialisert (Noen kan jobbe med heltall, andre flyttall, og andre er
kanskje mer generelle), og så vil kontrollogikken til CPU passe på at
rett instruksjon går til rett eksekveringsenhet. En super-skalar CPU kan
og kjøre instruksjonene i en annen rekkefølge (*out-of-order
eksekvering*) enn programmereren skrev de dersom kontrollogikken
oppdager at en eksekveringsenhet er ledig, og det er en instruksjon som
skal kjøres litt senere som kan kjøres der. Kontrollogikken jobber med å
holde mest mulig av enhetene i arkitekturen opptatt til enhver tid, slik
at flest mulig instruksjoner blir utført fortest mulig.

Moderne prosessorer bedriver også *spekulativ eksekvering*, dvs den
forsøker å gjette på hva som blir utfallet av branch-instruksjoner
(f.eks. jump-instruksjoner) og så reverserer hvis den tok feil. Både
out-of-order eksekvering og spekulativ eksekvering gjøres for å få
forbedret ytelse. Spekulativ eksekvering fikk mye oppmerksomhet i 2018
pga sårbarhetene [Spectre og
Meltdown](https://googleprojectzero.blogspot.no/2018/01/reading-privileged-memory-with-side.html).

Branch prediction demo:

    g++ -o bp bp.cpp
    ./bp
    # uncomment std::sort(data, data + arraySize);
    g++ -o bp bp.cpp
    ./bp

### HyperThreading/SMT

##### Hyperthreading/SMT

![image](img/Hyper-threaded_CPU.png)

(De fire fargene på boksene betyr instruksjoner fra fire forskjellige
program)

En videre utvidelse av super-skalar arkitekturen er Simultaneous
multithreading (SMT), eller Hyper-Threading (HT) som Intel kaller det.
For å øke mulighetene til å holde alle enhetene av CPU-en opptatt til
enhver tid er det mulig å utvide CPU til å holde to prosesser samtidig
(ved å ha dobbelt sett av alle registre programmer bruker (inkludert SP,
BP, IP etc.)). Da kan CPU til en hver tid plukke instruksjoner fra disse
to prosessene, alt etter hvilke eksekveringsenheter som er ledige til
enhver tid. En CPU med SMT/HT vil se ut som flere prosessorer for
operativsystemet (2 stk ved Intel’s HT). Konseptet hyperthreading kalles
også for "Virtuelle kjerner" i noen medier. Navnet der viser til at det
ser ut som om datamaskinen din har mange kjerner, men i realiteten er
det færre fysiske kjerner tilgjengelig.

Eksempel:  
Intel Core i7 2640M (<http://ark.intel.com/products/53464>) er en
tokjerneprosessor med hyperthreading. Denne har derfor fire tråder, noe
som gjør at OS tror at den har fire CPU-er:

``` 
  $ cat /proc/cpuinfo 
  processor       : 0
  vendor_id       : GenuineIntel
  model name      : Intel(R) Core(TM) i7-2640M CPU @ 2.80GHz
     ...
  processor       : 1
  vendor_id       : GenuineIntel
  model name      : Intel(R) Core(TM) i7-2640M CPU @ 2.80GHz
     ...
  processor       : 2
  vendor_id       : GenuineIntel
  model name      : Intel(R) Core(TM) i7-2640M CPU @ 2.80GHz
     ...
  processor       : 3
  vendor_id       : GenuineIntel
  model name      : Intel(R) Core(TM) i7-2640M CPU @ 2.80GHz
     ...
```

Demo

    time ./regn-5.bash
    time ./regn.bash 2 
    time ./regn.bash 4 
    time ./regn.bash 6
    time ./regn.bash 8

## Cache

For en CPU er RAM utrolig tregt. For å kunne unngå en del av ventingen
man får når en prøver å lese/skrive til RAM vil maskinvaren prøve å gå
til RAM så sjelden som mulig. Det handler f.eks om å lese større mengder
data fra RAM når en først leser fra ram for å så mellomlagre disse
dataene ett annet sted. Dette andre stedet er det man kaller for en
"cache".

NB\! Cache er ett veldig generisk begrep som benyttes mange steder i
moderne datamaskiner. Cache som blir beskrevet her i kapittel
[1.5](#sec:hw-review:cache) er en variant som er bygget fysisk i
maskinvare veldig nærme prosessorkjernen(e).

### Why cache?

##### Access times

![image](img/hw-2.png)

NB\!: Tallene i figuren er veldig cirkatall.

Andre eksempler på noen av disse tallene er [What Your Computer Does
While You
Wait](http://duartes.org/gustavo/blog/post/what-your-computer-does-while-you-wait)
og [Advanced Computer Concepts for the (Not So) Common Chef: Memory
Hierarchy: Of Registers, Cache and
Memory](https://software.intel.com/en-us/blogs/2015/06/11/advanced-computer-concepts-for-the-not-so-common-chef-memory-hierarchy-of-registers)

Flaskehalsen som oppstår mellom hastigheten på CPU og tiden det tar å
gjøre minneaksess kalles ofte *von Neumann flaskehalsen*, og i praksis
løses den med bl.a. cache-mekanismen.

##### Why Cache Works

  - Locality of reference
    
      - *spatial locality*
    
      - *temporal locality*

The smallest piece of data we can retrieve from memory is a Byte, but we
never cache just a single Byte, we cache a *cache line (typically 64
Byte)*

Cache gjør at programmer kjører raskere fordi instruksjoner gjenbrukes
ofte (samlokalisert i tid) mens data ofte aksesseres blokkvis
(samlokalisert i rom, har du lest en byte skal du sikkert snart ha
byte’n ved siden av den også).

### Write Policy

##### Write Policy

  - Write-through  
    Write to cache line and immediately to memory

  - Write-back  
    Write to cache line and mark cache line as *dirty*

Ved write-back skrives dataene til minne først når cache line’n skal
overskrives av en annen, eller i andre tilfeller som f.eks. en context
switch.

*Et viktig poeng er at Write-back cacher er spesielt utfordrende for når
det er flere prosessorkjerner tilstede: hva hvis flere prosessorkjerner
har cachet de samme dataene? Hvordan vet en prosessorkjerne at ikke en
annen prosessorkjerne har skrevet til de dataene den har cachet?* Dette
løses selvfølgelig med en *cache coherence protokoll*, f.eks. MESI.

#### Write-through

![image](img/Writethrough.png)

#### Write-back

![image](img/Writeback.png)

Begge figurene er hentet fra
<http://en.wikipedia.org/wiki/Cache_(computing)>

Ved Write-back caching må vi altså sjekke om cache-blokken vi ønsker
cache i er dirty, dvs inneholder data som ikke er skrevet til neste nivå
datalagring enda. Dette gjelder både for lese og skrive forespørsler.
Write through caching er det enkleste og tryggeste (siden caching bare
vil inneholde kopi av data som finnes et annet sted), men skal systemet
gi god ytelse for skriveforespørsler så må man benytte write-back
caching.

*I operativsystemer er det et poeng at når vi bytter om fra å jobbe på
et program til et annet (context switch) så er cachen full av data fra
det første og det tar tid å bytte det ut\! Derav sier vi at context
switch er ganske kostbart.*

## Review questions and problems

1.  Hva er et “Directive” i assemblykode?

2.  Hva forbinder du med begrepene *superskalar* og *pipelining* i en
    prosessor?

3.  Hva er oppgaven til en C kompilator? Hva er forskellen mellom
    C-kode, assemblykode og maskinkode?

4.  Forklar hva hver linje i følgende assemblykode gjør:
    
        01         .text   
        02 .globl main
        03 main:   
        04         pushq   %rbp
        05         movq    %rsp, %rbp
        06         movl    $0, -4(%rbp)
        07         cmpl    $0, -4(%rbp)
        08         jne     .L2 
        09         addl    $1, -4(%rbp)
        10 .L2:    
        11         movl    $0, %eax
        12         popq    %rbp
        13         ret
    
    Denne assemblykoden ble generert av et C-program på ca fem linjer.
    Hvordan så programkoden til dette C-programmet ut? (Hint: Løs denne
    ved å prøve deg frem med å lage enkel C-kode som du kompilerer til
    assemblykode og sammenlikner med koden over, sjekk gjerne ut
    [Compiler Explorer](https://godbolt.org) til dette.)

5.  Forklar hva hver linje i følgende assemblykode gjør:
    
        01         .text   
        02 .globl main
        03 main:   
        04         pushq   %rbp
        05         movq    %rsp, %rbp
        06         movl    $0, -4(%rbp)
        07         jmp     .L2 
        08 .L3:    
        09         addl    $1, -4(%rbp)
        10         addl    $1, -4(%rbp)
        11 .L2:    
        12         cmpl    $9, -4(%rbp)
        13         jle     .L3 
        14         movl    $0, %eax
        15         popq    %rbp
        16         ret
    
    Denne assemblykoden ble generert av et C-program på ca fem linjer.
    Hvordan så programkoden til dette C-programmet ut?

## Lab tutorials

1.  **Measuring run times**
    
    1.  Clone iikos-files if you haven’t done so already, and `cd` to
        the directory where you find `mlab.c`
        
        ``` 
          git clone https://gitlab.com/erikhje/iikos-files.git
          cd iikos-files/01-hwreview
        ```
    
    2.  Set the bash reserved word `time` to output only the elapsed
        time in seconds with
        
            TIMEFORMAT="%R"
    
    3.  In `g_x[j][i]` (on line 10), try all four possible combinations
        of `i` and `j`:
        
            g_x[j][i] = g_x[i][j] * 1;
            g_x[i][j] = g_x[i][j] * 1;
            g_x[j][i] = g_x[j][i] * 1;
            g_x[i][j] = g_x[j][i] * 1;
        
        For each combination do
        
            gcc -o mlab mlab.c
            for i in {1..10}
            do 
              (time ./mlab) |& tr -d '\n' | tr ',' '.' >> loopidx.dat
              echo -n ' ' >> loopidx.dat
            done
            echo >> loopidx.dat
    
    4.  Verify that you now have a file with four rows of ten data
        points
        
            cat loopidx.dat
    
    5.  Let’s use Python to read the data file and create a nice
        PDF-figure
        
            # start the python interpreter
            python
            
            # copy and paste the following into the interpreter
            # (or put this in a file a.py and run it with python a.py)
            import matplotlib.pyplot as plt
            import numpy as np
            
            fig = plt.figure()
            plt.ylabel('time')
            plt.xlabel('events')
            plt.grid(True)
            plt.xlim(0,9)
            plt.ylim(0,3)
            
            a=np.loadtxt('loopidx.dat')
            
            plt.plot(a[0,:], label = "line 0")
            plt.plot(a[1,:], label = "line 1")
            plt.plot(a[2,:], label = "line 2")
            plt.plot(a[3,:], label = "line 3")
            plt.legend()
            
            fig.savefig('loopidx.pdf')
            
            # exit with CTRL-D
    
    6.  View you newly create file `loopidx.pdf` (open it with `evince`
        or in your browser, or the pdf-viewer of your choice)

# Operating Systems and Processes

## Introduction

##### What does the OS do?

The operating system

  - virtualizes  
    physical resources so they become user friendly

  - manages  
    the resources of a computer

Virtualizing the CPU

``` 
    ./cpu A
    ./cpu A & ./cpu B & ./cpu C & ./cpu D &
```

Virtualizing memory

``` 
    setarch $(uname --machine) --addr-no-randomize /bin/bash
    ./mem 1
    ./mem 1 & ./mem 1 &
```

Concurrency

``` 
    ./threads 1000
    ./threads 10000
```

Persistance

``` 
    ./io
    ls -ltr /tmp
    cat /tmp/file
```

## Design goals

##### OS design goals

  - Virtualization  
    create abstractions

  - Performance  
    minimize overhead

  - Security  
    protect/isolate applications

  - Reliability  
    stability

  - Energy-efficient  
    environmentally friendly

## History

See [Éric Lévénez’s site](https://www.levenez.com).

##### Before 1970

  - 1940-55  
    Direct machine code, moving wires

  - 1955-65  
    Simple OS’s, punchcards

  - 1965-70  
    Multics, IBM OS/360 (the mainframe)

##### Unix/Linux

  - Ken Thompson developed a stripped-down version of MULTICS on a PDP-7
    he got hold of in 1969

  - A large number of flavors developed (SystemV or Berkely-based)

  - The *GNU*-project started in 1983 by Richard Stallman

  - Unified with *POSIX* interface specification in 1985

  - Minix in 1987 inspired Linus Torvalds to develop *Linux* (released
    in 1991)

##### Windows

  - IBM sold PCs bundled with MS-DOS from beginning of 80s

  - DOS/Windows from 85-95

  - Win95/98/Me from 95-2000

  - *WinNT*, 2000, XP, Vista, 7, 8, 10 from 93-

  - *WinNT*, 2000, 2003, 2008, 2012, 2016, 2019 from 93-

##### Unix/Linux

Separate policy and mechanism

##### Unix/Linux

Process vs program

##### Unix/Linux

Process creation, fig 4.1 (merk: stack)

##### Unix/Linux

Process states, fig 4.2

##### Unix/Linux

Using the CPU, fig 4.3, 4.4

##### Unix/Linux

What the OS stores about processes fig 4.5 (Process/task list/table,
PCB)

Study cpu-intro/README.md then do lab

``` 
    ps aux | awk '{print $8}' | grep -P '^S' | wc -l
    ps aux | awk '{print $8}' | grep -P '^R' | wc -l
    ps aux | awk '{print $8}' | grep -P '^I' | wc -l
    # NO, inefficient command line usage, 
    # always try to filter as far left as you can
    ps -eo stat | grep -P '^S' | wc -l
```

[Where does the `I` come
from?](https://unix.stackexchange.com/questions/412471/what-does-i-uppercase-i-mean-in-ps-aux)

## Review questions and problems

1.  Forklar kort hva som er de to hovedoppgavene til et operativsystem.

2.  What are the design goals for operating systems?

3.  What is batch processing?

4.  What information do you find in the process list / process table?

5.  Do the “Homework (Simulation)” exercises in chapter four. Read the
    [README
    file](https://github.com/remzi-arpacidusseau/ostep-homework/blob/master/cpu-intro/README.md)
    first. Feel free to join together with other students when you do
    this, and discuss each question.

## Lab tutorials

1.  **Kommandolinje Unix/Linux**  
    Bli kjent med kommandolinje Unix/Linux hvis du ikke er godt kjent
    med den fra før. Gjennomfør tutorial en til fem på
    <http://www.ee.surrey.ac.uk/Teaching/Unix/index.html> (les også
    gjennom “introduction to the UNIX operating system” før første
    tutorial). Merk: i del 2.1 av denne tutorialen så bes du om å
    kopiere en fil `science.txt`. Denne filen finnes ikke hos deg, så
    last istedet den ned med kommandoen  
    `wget http://www.ee.surrey.ac.uk/Teaching/Unix/science.txt`  
    *MERK: Neste uke forventes det at du kan alle disse kommandoene i
    fingrene\!*
    
    Hvis du vil lære Linux på en litt mer moderne og kanskje morsommere
    måte så prøv [Linux Journey](https://linuxjourney.com/).

2.  Bli kjent med programvarepakkesystemet til Debian-baserte
    distribusjoner (lærer går gjennom dette).

3.  Bli kjent med en teksteditor. Hvis du er logget inn på en
    linux-server via ssh så kan du bruke `nano` som viser deg en meny
    nederst på skjermen med hva du kan gjøre. Hvis du har Linux med GUI
    (f.eks. Ubuntu i en virtuell maskin) og ønsker å gjøre det enklest
    mulig, bare bruk `gedit` som er som notepad. Hvis du vil lære deg en
    teksteditor som alle systemadministratorer må kunne, så kan du bruke
    anledning til å lære deg `vi` med denne utmerkede tutorial:
    <http://heather.cs.ucdavis.edu/~matloff/UnixAndC/Editors/ViIntro.pdf>.

4.  **C-programmering på Linux**  
    Lag deg en directory `hello` og i denne lag en fil `hello.c` som
    inneholder:
    
        #include <stdio.h>
        int main(void) {
          printf("hello, world\n");
          return 0;
        }
    
    Kompiler denne til en kjørbar (eksekverbar) fil med `gcc -Wall -o
    hello hello.c` (`-Wall` betyr Warnings:All og er ikke nødvendig, men
    kjekk å ta med siden den hjelper oss programmere nøyere) og kjør den
    med `./hello`. Kjør den også ved å angi full path (dvs start
    kommandoen med `/` istedet for `./`). Sjekk om environment
    variabelen din `PATH` inneholder en katalog `bin` i hjemmeområdet
    ditt (`echo $PATH`). Hvis den ikke gjør det, så lag `bin` (`mkdir
    ~/bin`) hvis den ikke finnes, og legg til den i path’n med
    `PATH=$PATH:~/bin` (gjør evn endringen permanent ved å editere filen
    `~/.bashrc`). Kopier `hello` til `bin` og kjør programmet bare ved å
    skrive `hello`.
    
    Hvis du ikke har programmert i C før, så les gjennom f.eks. denne  
    [Learn C Programming, A short C
    Tutorial](http://www.loirak.com/prog/ctutor.php)
    
    Det anbefales også å se videoen [Writing a simple Program in C - bin
    0x02 (LiveOverflow)](https://www.youtube.com/watch?v=JGoUaCmMNpE)
    
    Sørg for å ha gode rutiner for kvalitetssikring av koden din, f.eks.
    statisk kodeanalyse med `clang-tidy`  
    `clang-tidy -checks='*' kode.c --`
    
    Egentlig burde vi også lese denne  
    [How to C in 2016](https://matt.sh/howto-c) og denne  
    [Modern C](http://icube-icps.unistra.fr/index.php/File:ModernC.png)
    samt være klar over hva som finnes her  
    [SEI CERT C Coding
    Standard](https://wiki.sei.cmu.edu/confluence/display/c/SEI+CERT+C+Coding+Standard)

5.  **Litt mer C-programmering og kodekvalitet**  
    Klipp og lim inn det siste eksempelet under “3. Loops and
    Conditions” fra [Learn C Programming, A short C
    Tutorial](http://www.loirak.com/prog/ctutor.php) og kjør  
    `gcc -Wall kode.c`  
    `clang-tidy -checks='*' kode.c --`  
    Klarer du forbedre koden ut fra hva gcc and clang-tidy forteller
    deg?
    
    Vi skal ikke bli ekspert-programmerere i C i dette emnet, vi skal
    bare bruke C littegrann for å lære oss om operativsystemer, men det
    er viktig at vi blir vant til å sjekke kodekvaliteten vår nå som vi
    har programmert i flere emner allerede. I dette emnet er det greit
    at vi ikke alltid skrive optimal og sikker kode (siden det fort blir
    mange ekstra kodelinjer som ikke nødvendigvis hjelper oss lære
    operativsystemer bedre), MEN VI MÅ ALLTID VÆRE BEVISSTE PÅ AT KODEN
    VÅR KAN HA SVAKHETER/SÅRBARHETER.

# System Calls

## System Calls

### `fork()`

##### `fork()`

Fig 5.1

``` 
    cat p1.c
    make
    ./p1
```

The big punchline: *The return code `rc` is `0` in the newly created
child process, while `rc` contains the value of the child’s process-ID
in the parent process.* You can use this in your C-code to write
separate code for parent and child processes.

It looks like the parent process will always run before the newly
created child process, but you have noe guarantee for this. Sometimes
the parent process will run after the child process. If you dont believe
this try to run the program 10000 times to test:

``` 
    for i in {1..10000}
    do 
        if [[ ! -z "$(./p1 | tail -n 1 | grep parent)" ]]
        then 
            echo "parent last in run $i"
        fi
    done
```

btw, ask teacher or a fellow student what happens in the test  
`! -z "$(./p1 | tail -n 1 | grep parent)"`

### `wait()`

##### `wait()`

Fig 5.2

``` 
    diff p1.c p2.c
    apt install colordiff
    colordiff p1.c p2.c
    ./p2
```

In p2.c parent will always run last because of synchronization
introduced with the system call `wait()`.

### `exec()`

##### `exec()`

Fig 5.3

``` 
    ./p3
```

### Why???

##### Why fork-exec?

Fig 5.4

``` 
    ./p4
```

Why not just like `CreateProcess()` on Windows?

### Signals

##### Signal a Process

``` 
    man kill
    man 7 signal # search for
                 # 'Standard'
```

## Process execution

### Direct execution

##### Direct execution protocol

Fig 6.1

### Restricted operation

##### System Calls

![image](img/syscalls.png)

One of the two main tasks of the operating system is to create a
beautiful/nice/easy-to-program interface between the application and the
hardware (the other main task is to manage the hardware). This interface
is composed of a set of system calls, while the interface directly
against the hardware is composed of a set of machine instructions
(e.g. the X86-instructions).

Note: Don’t be fooled by this illustration. The interfaces are not hard
borders that cannot by bypassed. It is possible for the application to
sometimes talk directly to the hardware (e.g. use some of the
X86-instructions), but most of the times this illustration makes sense
since the application asks to operating system to talk to the hardware
on its behalf.

##### Terminology

  - user mode (application)

  - kernel mode (operating system)

  - mode switch (between user and kernel mode)

  - context switch (between processes)

##### Limited direct execution protocol

Fig 6.2

*Trap table* is in principle the same as [Interrupt vector
table](https://en.wikipedia.org/w/index.php?title=Interrupt_vector_table&oldid=944841296).

##### When does the OS run?

Three types of traps/interrupts:

  - (Trap) Software interrupt/System Call (synchronous)

  - (Trap) Exception (synchronous)

  - Hardware interrupt (asynchronous)

Unfortunately there is not a consistant terminology, but most of the
time trap is used for system calls and exceptions, while interrupt is
always used for hardware interrupt. Traps/Interrupts are events that
give the operating system control. Synchronous means that it happens as
a consequence of an instruction (e.g. a process try to divide by zero,
this would trigger an exception). Asynchronous means it does not happen
as a consequence of anything predictable, it just happens because a
packet arrived on the network interface, or the user suddenly moved the
mouse.

### Timer interrupt

##### With timer interrupt

Fig 6.3

`strace -c ls`

##### A Simple Example

    .data
    str:
    .ascii "hello world\n"
    .text
    .global _start
    _start:
    movq $1, %rax   # use the write syscall
    movq $1, %rdi   # write to stdout
    movq $str, %rsi # use string "Hello World"
    movq $12, %rdx  # write 12 characters
    syscall         # make syscall
    
    movq $60, %rax  # use the _exit syscall
    movq $0, %rdi   # error code 0
    syscall         # make syscall

Se fil `asm-syscall-2017.s` for litt mer kommentarer. Se også klassisk
systemkall med bruk av assembly instruksjonen `int 0x80` dvs vi
"genererer et interrupt nr 80" i filen `asm-syscall.s`. Idag brukes ikke
`int 0x80` instruksjonen siden `syscall` er kjappere.

Se gjennomgang av systemkallmekanismen ved å se de seks første minuttene
av [Syscalls, Kernel vs. User Mode and Linux Kernel Source Code - bin
0x09](https://www.youtube.com/watch?v=fLS99zJDHOc)

Sjekk hva som gjøres med libc wrapper og uten:

    gcc -o asm-syscall asm-syscall.s -no-pie
    strace -c ./asm-syscall
    sed -i 's/main/_start/g' asm-syscall.s
    gcc -o asm-syscall asm-syscall.s -nostdlib -no-pie
    strace -c ./asm-syscall

Les om `syscall` ca side 1320 i [Intel 64 and IA-32 Architectures
Software Developer Manual: Vol
2](http://www.intel.com/content/www/us/en/architecture-and-technology/64-ia-32-architectures-software-developer-instruction-set-reference-manual-325383.html)

## Review questions and problems

1.  DUMMY

2.  DUMMY

## Lab tutorials

1.  DUMMY

# Scheduling

## Turnaround time

##### Assumptions (we have to break)

1.  Each job runs for the same amount of time.

2.  All jobs arrive at the same time.

3.  Once started, each job runs to completion.

4.  All jobs only use the CPU (i.e., they perform no I/O)

5.  The run-time of each job is known.

##### Turnaround time

The time from a process enters the system until it leave, e.g.

``` 
        time uuidgen
```

Remember the process states (ready, running, blocked)

### FIFO

##### First In First Out (FCFS)

Fig 7.1

\- What kind of workload could you construct to make FIFO perform
poorly?

Fig 7.2

Search the Internet for "Convoy effect".

### SJF

##### Shortest Job First

Fig 7.3

\- If we now assume that jobs can arrive at any time, what problems does
this lead to?

Fig 7.4

### STCF

##### Shortest Time to Completion First

Fig 7.5

\- While great for turnaround time, this approach is quite bad for
response time and interactivity.

## Response time

##### Response time

The time from a process enters the system until it runs for the first
time

### Round Robin

##### Round Robin

Fig 7.7

  - time slice/quantum

  - what is the real cost of a context switch?

### Overlap

##### Always overlap

Fig 7.9

## MLFQ

### Basics

##### Basics

Fig 8.1

  - Rule 1  
    If Priority(A) \> Priority(B), A runs (B doesn’t).

  - Rule 2  
    If Priority(A) = Priority(B), A & B run in RR

### Priority

##### Priority

  - Rule 3  
    When a job enters the system, it is placed at the highest priority
    (the topmost queue).

  - Rule 4a  
    If a job uses up an entire time slice while running, its priority is
    reduced (i.e., it moves down one queue).

  - Rule 4b  
    If a job gives up the CPU before the time slice is up, it stays at
    the same priority level.

Fig 8.2-8.4

### Boost

##### Boost

  - Rule 4  
    Once a job uses up its time allotment at a given level (regardless
    of how many times it has given up the CPU), its priority is reduced
    (i.e., it moves down one queue).

  - Rule 5  
    After some time period S, move all the jobs in the system to the
    topmost queue.

Fig 8.5-8-7

## Fair Share

### Lottery

##### Lottery and Randomness

See "Tip: Use randomness"

## Multiprocessor

### Affinity

##### Cache Coherence and Affinity

Fig 10.2

### Gang Scheduling

##### Gang Scheduling

Should threads from the same process (or processes from the same virtual
machine) run at the same time on the CPUs?

This is a hard question, it depends on the workload, meaning it only
makes sense of the threads communicate a lot between each other. We’ll
get back to this when we talk about synchronization.

## Review questions and problems

1.  DUMMY

2.  DUMMY

## Lab tutorials

1.  DUMMY

# Address Spaces and Address Translation

## Address space

##### Multiprogramming

  - Fig 13.1

  - Saving from memory to disk timeconsuming…

  - Fig 13.2

##### Address Space

  - Fig 13.3

> Virtualized memory because the program is not loaded in memory where
> it thinks it is.

##### More exact

![image](img/memorylayout.png)  
<span>[CC-BY-SA-3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en)
by Dougct</span>

##### Goals

  - Transparancy  
    it should just happen behind the scene

  - Efficiency  
    in time and space

  - Protection  
    isolated from other address spaces (*security*)

## Memory: API

##### Stack vs Heap Memory

  - Automatic memory on stack  
    `int x;`

  - Heap manually allocated  
    `int *x = (int *) malloc(sizeof(int));`

  - (Global variables in Data segment, not in Heap)

##### Free Memory

  - `free(x);`

  - Easy to make mistakes\! `valgrind` (`purify`)

  - Search the Internet for "use after free"

## Address Translation

##### Relocating

  - Fig 15.1

  - Fig 15.2

##### Base and Bound/Limit Register

  - Fig 15.3

##### OS Responsibilities

  - Fig 15.4

##### Execution

  - Fig 15.5

  - Fig 15.6

## Segmentation

##### Segments

Solve the problems with one set of base and bounds/limits registers for
each segment

  - Fig 16.1

  - Fig 16.2

## Free Space Mgmt

##### Free Space Management

Strategies for all storage (memory and disks)

  - Free list

  - Bitmap

Unavoidable problems:

  - *External fragmentation* (problem for variable sized units)

  - *Internal fragmentation* (problem for fixed sized unit)

## Paging

##### Terminology

  - Paging  
    divide space into fixed size units/pieces/chunks/slots

  - Page  
    a fixed sized unit

  - Page frame  
    a page in physical memory (RAM)

##### Virtual and Physical Space

  - Fig 18.1 Virtual address spaces

  - Fig 18.2 Physical address space

##### Address Translation

  - Fig 18.3 What the MMU does

  - Fig 18.4 Page table in physical memory

##### Page Table Entry

  - Fig 18.5

##### Example Memory Trace

  - Fig 18.7 Do you understand what is going on here?

## Review questions and problems

1.  DUMMY

2.  DUMMY

## Lab tutorials

1.  DUMMY

# Memory Management

## Faster Translations

Pagin is a wonderful mechanism but we have two problems:

1.  It’s to slow, every memory access (also called a memory reference)
    leads to an extra memory access since the page table is stored in
    RAM, let’s solve this with TLB

2.  The page table is to big (takes up too much space in RAM), let’s
    solve this with one of
    
    1.  Multi-level page table (most used)
    
    2.  Inverted page table

### TLB

##### Translation Lookaside Buffer (TLB)

  - TLB is a CPU cache, one of the caches we talk about when we say L1,
    L2, L3 cache

  - `cpuid -1 | less # search for TLB`

  - Fig 19.1 pseudo code

<!-- end list -->

    -+  Virtual (logical) address
    C|  +-------------+
    P|->|pagenr|offset|
    U|  +-------------+
    -+     |
           |      pagenr framenr
           |     +--------------+
           |  +->|      |       |
           |  +->|      |       |
           |  +->| Translation  |TLB hit
           +--+->| Lookaside    |------+
              +->| Buffer       |      |
              +->|      |       |      |              
              +->|      |       |      |              +--------+
              +->|      |       |      |              |        |
              |  +--------------+      |              |        |
              |                        | Physical     |Physical|
              |TLB miss                v address      |memory  |
              |                    +--------------+   | (RAM)  |
              |                    |framenr|offset|-->|        |
              |                    +--------------+   |        |
              |     +-----+            ^              |        |
              |     |Page |            |              +--------+
              +---->|Table|------------+
                    |     |
                    +-----+

##### Hit or Miss?

  - Fig 19.2, accessing this array in sequence

  - *miss*, hit, hit, *miss*, hit, hit, hit, *miss*, hit, hit

  - 70% hit rate

##### Why Cache?

  - Spatial locality

  - Temporal locality

*Unfortunately fast caches need to be small because of physics…*

##### OS or HW?

  - Fig 19.3, OS handles TLB (RISC)

  - On X86, HW handles TLB (CISC)

### ASID

##### What is in a TLB entry?

  - A copy of the Page Table Entry (PTE)

  - Address Space Identifier (ASID) on modern architectures, to avoid
    TLB flush on every context switch

## Smaller Page Tables

An 32-bit address space with 4KB pages (12-bit offset), with 32-bit
(4B\(=2^{2}\)B) page table entries:
\[\frac{2^{32}}{2^{12}}\times2^{2}\mbox{B}=2^{22}\mbox{B}=4\mbox{MB}\]
With a couple of hundred processes, we can’t have each process use 4MB
just for its page table, and what about todays 64-bit address spaces…

##### Bigger pages?

  - When the result of a division is too big, one can
    
    1)  decrease the numerator (teller) or
    
    2)  increase the denominator (nevner)

  - Bigger pages is increasing the denominator

  - X86 supports page sizes of 4KB, 2MB or 1GB

  - Bigger pages leads to more *internal fragmentation*

### Multi-level PT

##### Multi-level Page Table

  - Fig 20.3

<!-- end list -->

  - PTBR  
    Page Table Base Register (CR3 on X86)

  - PDBR  
    Page Directory Base Register (CR3 on X86)

##### X86-32bit

![image](img/X86_Paging_4K.png)
<span>[RokerHRO](https://commons.wikimedia.org/wiki/File:X86_Paging_4K.svg),
"X86 Paging 4K", [CC
BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/legalcode)</span>

##### X86-64bit

![image](img/X86_Paging_64bit.png)
<span>[RokerHRO](https://commons.wikimedia.org/wiki/File:X86_Paging_64bit.svg),
"X86 Paging 64bit", [CC
BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/legalcode)</span>

### Inverted PT

##### Inverted Page Table

> Here, instead of having many page tables (one per process of the
> system), we keep a single page table that has an entry for each
> physical page of the system

*Cannot lookup, have to search the table for the entry…*

## Memory Management

### Swap Space

##### Swap Space

  - Fig 21.1

  - How big is your swap space?

  - Binaries (executables/libraries) don’t need swap space

### Page Fault

##### Page Fault terminology

  - TLB miss / Soft miss  
    Page Table Entry (PTE) is not TLB.

  - Minor page fault / Soft miss / Soft (page) fault  
    Page is in memory but not marked as present in PTE (e.g. a shared
    page brought into memory by another process)

  - Major page fault / Hard miss / Hard (page) fault  
    Page is not in memory, I/O required.

<!-- end list -->

    \time -v gimp
    \time -v gimp
    sync ; echo 3 | sudo tee /proc/sys/vm/drop_caches
    \time -v gimp

##### Tip

  - See box "Tip: Do work in the background"

## Page Replacement Policies

##### Parallell Problems

  - CPU caches speed up RAM access

  - RAM speeds up (SSD) disk access

  - SSD can speed up access to RAID (HDD) array

  - RAID controllers have RAM to speed up array access

  - …

*It’s all "cache management"*

##### ?

.

##### ?

.

## Review questions and problems

1.  DUMMY

2.  DUMMY

## Lab tutorials

1.  DUMMY

# Threads

## Introduction

##### ?

.

.

## Review questions and problems

1.  DUMMY

2.  DUMMY

3.  DUMMY

## Lab tutorials

1.  DUMMY

# Locks and Conditional Variables

## Introduction

##### ?

.

.

## Review questions and problems

1.  DUMMY

2.  DUMMY

3.  DUMMY

## Lab tutorials

1.  DUMMY

# Semaphores and Concurrency Problems

## Introduction

##### ?

.

.

## Review questions and problems

1.  DUMMY

2.  DUMMY

3.  DUMMY

## Lab tutorials

1.  DUMMY

# Input/Output and RAID

## Introduction

##### ?

.

.

## Review questions and problems

1.  DUMMY

2.  DUMMY

3.  DUMMY

## Lab tutorials

1.  DUMMY

# Fil Systems

## Introduction

##### ?

.

.

## Review questions and problems

1.  DUMMY

2.  DUMMY

3.  DUMMY

## Lab tutorials

1.  DUMMY

# Data Integrity and SSD

## Introduction

##### ?

.

.

## Review questions and problems

1.  DUMMY

2.  DUMMY

3.  DUMMY

## Lab tutorials

1.  DUMMY

# Virtualization and Containers

## Introduction

##### ?

.

.

## Review questions and problems

1.  DUMMY

2.  DUMMY

3.  DUMMY

## Lab tutorials

1.  DUMMY
