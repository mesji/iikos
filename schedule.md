# About the Course
## Expected background
We will use Linux command line and C-programming in this course, see the [CIS-document](https://gitlab.com/erikhje/iikos/-/blob/master/CIS/README.md) for information about Linux (NTNU-students can also just login (ssh) to `login.stud.ntnu.no` for most exercises).
* BIDATA know programming but not C, have some Linux experience but also have IIKG1001 in parallell where Linux is covered in the first weeks.
* BPROG know C-programming and some Linux from IIKG1001.
* DIGSEC know C-programming and Linux from DCSG1001.
* CIS know some C-programming, some computer architecture and some Linux/Python from Raspberry PI in the networks course.
## Course setup
| Time&Place | Happening |
| ---- | ------------- |
| Tuesdays 0815-1000 A255/A254 | Lab/Exercises BIDATA,BPROG with Sonja and Håkon |
| Tuesdays 1215-1000 S206 | Lecture ALL with Erik |
| Tuesdays 1415-1600 A255/A254 | Lab/Exercises CIS with Eirik and Håkon |
| Thursdays 0915-1100 A255/A254 | Lab/Exercises DIGSEC with Sonja and Simon |
| Thursdays 1115-1300 Teams | Lab/Exercises BIDATA,BPROG,DIGSEC with Simon and Eirik |
| Thursdays 1215-1300 S206 | Lecture/Spørretime BIDATA,BPROG,DIGSEC with Erik |

([See tp](https://tp.uio.no/ntnu/timeplan/timeplan.php?id=IDATG2202&type=course&sort=form&sem=20h) for official schedule of course).

Every week we have two hours of lectures plus one additional session for CIS at Jørstadmoen and one additional session for NTNU-students. In addition the teaching assistants are available for four hours per week (two hours scheduled in class rooms, two hours online in Teams). All the two-hour lectures are [available in Omnom](https://forelesning.gjovik.ntnu.no/publish/index.php?lecturer=all&topic=IDATG2202+Operativsystemer). In addition there will be video lectures available in the weekly schedule below.
## Download course material
Clone the git repos (remember you can update any time with `git pull`):
```sh
git clone https://github.com/remzi-arpacidusseau/ostep-code.git
git clone https://github.com/remzi-arpacidusseau/ostep-homework.git
git clone https://gitlab.com/erikhje/iikos-files.git # demos and example files
git clone https://gitlab.com/erikhje/iikos.git       # compendia and course docs
```
## How to get a good grade
In prioritized order:
1. Do all the Lab exercises and Review questions and problems in [the compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf).
1. Read the compendia and the weekly chapters in the textbook as stated in the weekly schedule below. _When you read in the textbook, pay attention to what is included in the compendia and spend your time reading the parts in the textbook that have focus in the compendia_.
1. Watch all the lectures.
## Weekly schedule
| Week | Topic (Chapters) | Additional Info |
| ---- | -------------    | --------------- |
| 34   | Computer architecture ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 1, [potensregning](https://gitlab.com/erikhje/iikos/-/blob/master/potensregning.pdf))| Video-lecture: [computer architecture part 3](https://youtu.be/gHlDm6aBBfY) |
| 35   | Introduction and processes ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 2, textbook chp [2](http://pages.cs.wisc.edu/~remzi/OSTEP/intro.pdf), [4](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-intro.pdf)) |  |
| 36   | System calls ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 3, textbook chp [5](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-api.pdf), [6](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-mechanisms.pdf))|  |
| 37   | Scheduling ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 4, textbook chp [7](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-sched.pdf), [8](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-sched-mlfq.pdf), [9.1](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-sched-lottery.pdf), [10.1](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-sched-multi.pdf), [10.3](http://pages.cs.wisc.edu/~remzi/OSTEP/cpu-sched-multi.pdf)) |  |
| 38   | Address spaces and paging ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 5, textbook chp [13](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-intro.pdf), [14](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-api.pdf), [15](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-mechanism.pdf), [16.1, 16.4](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-segmentation.pdf), [17.1](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-freespace.pdf), [18](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-paging.pdf)) |  |
| 39   | Memory management ([compendia](https://gitlab.com/erikhje/iikos/-/blob/master/compendia.pdf) chp 6, textbook chp [19](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-tlbs.pdf), [20](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-smalltables.pdf), [21](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-beyondphys.pdf), [22](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-beyondphys-policy.pdf), [23.2](http://pages.cs.wisc.edu/~remzi/OSTEP/vm-complete.pdf)) |  |
| 40   | Threads (26, 27) |  |
| 41   | Locks, conditional variables (28, 29, 30) |  |
| 42   | Semaphores, concurrency problems (31, 32) |  |
| 43   | I/O, RAID (36, 37.1-37.4, 38)  |  |
| 44   | File systems (39, 40) |  |
| 45   | FSCK/journalling, SSD (42, 44) |  |
| 46   | VMs and containers (separate document) |  |
| 47   | Repetition |  |

<!---
IF WE TRY TO SQUEEZE IN COMPUTER ARCHITECTURE IT WILL BE TOO TIGHT:

| Week | Topic (Chapters) | Exercises | Additional Resources |
| ---- | -------------    | --------- | -------------------- |
| 34   | Arch, X86 Assembly, Registers | | |
| 35   | CPU-details, Multicore/SMT | | |
| 36   | Cache, I/O, Interrupts | | |
| 37   | Intro, Process (2, 4) |  |  |
| 38   | SysCalls (5, 6)|  |  |
| 39   | Scheduling (7, 8, 9.1, 10.1, 10.3) |  |  |
| 40   | Address spaces/translation, C (13, 14, 15) |  |  |
| 41   | Segmentation and paging (16, 17, 18, 19) |  |  |
| 42   | Memory management (20, 21, 22) |  |  |
| 43   | Threads, locks, conditional variables (26, 27, 28, 29, 30) |  |  |
| 44   | Semaphores, concurrency problems (31, 32) |  |  |
| 45   | I/O, RAID (36, 37.1-37.4, 38)  |  |  |
| 46   | File systems, FSCK/journalling, SSD (39, 40, 42, 44) |  |  |
| 47   | Virtualization and containers (separate document) |  |  |
--->
